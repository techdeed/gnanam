<?php
/**
 * Component By Techdeed
 * 
 * PHP version 5
 * 
 * @category  Controller
 * @package   Com_School
 * @author    Gnanakeethan Balasubramaniam <gnanakeethan@gmail.com>
 * @copyright 2012 Gnanakeethan.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 * @version   GIT:<git_id>
 * @link      Techdeed.x10.mx
 * @since     0.0.1.0.a.0.0.1.a    
 */


// No direct access
defined('_JEXEC') or die;

/**
 * SchoolController
 * 
 * Base controller for Gnanam School Management Component
 * 
 * @category   Controllers
 * @package    Com_School
 * @subpackage School
 * @author     Gnanakeethan Balasubramaniam <gnanakeethan@gmail.com>
 * @copyright  2012 Gnanakeethan.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       Techdeed.x10.mx
 */
class SchoolController extends JController
{
	/**
	 * Method to display a view.
	 *
	 * @param	boolean			$cachable	If true, the view output will be cached
	 * @param	array			$urlparams	An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return	JController		This object to support chaining.
	 * @since	1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		$view		= JFactory::getApplication()->input->getCmd('view', 'school');
        JFactory::getApplication()->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}
}
