<?php

/**
 *  Component By Techdeed
 * 
 * PHP version 5
 * 
 * @category  Library 
 * @package   Com_School
 * @author    Gnanakeethan Balasubramaniam <gnanakeethan@gmail.com>
 * @copyright 2012 Gnanakeethan.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 * @version   GIT:<git_id>
 * @link      Techdeed.x10.mx
 * @since     0.0.1.0.a.0.0.1.a *   
 */

// No Direct Access.
defined('_JEXEC') || die;

/**
 * BarChart
 * 
 * BarChart of com_school components library
 * 
 * @category  Library
 * @package   Com_School
 * @author    Gnanakeethan Balasubramaniam <gnanakeethan@gmail.com>
 * @copyright 2012 Gnanakeethan.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 * @link      Techdeed.x10.mx
 */
class SchoolViewChartsBar extends SchoolViewCharts {
    
          
}
