<?php

class SchoolLessCompiler {
    
    public function autoCompileLess($input,$output) {     
      
        $in_dir='components/com_school/assets/less/';
        $out_dir='components/com_school/assets/css/school/';
        $inputFile=$in_dir.$input;
        $outputFile=$out_dir.$output;
        $cacheFile = $inputFile.".cache";
        require 'lessc.inc.php';
        $less= new lessc();
        $less->setFormatter("compressed");
        if (file_exists($cacheFile)) {
            $cache = unserialize(file_get_contents($cacheFile));
        } else {
            $cache = $inputFile;
        }
        
        $less = new lessc;
        $less->setFormatter("compressed");
        $newCache = $less->cachedCompile($cache);

        if (!is_array($cache) || $newCache["updated"] > $cache["updated"]) {
            file_put_contents($cacheFile, serialize($newCache));
            file_put_contents($outputFile, $newCache['compiled']);
        }
    }
}