<?php
/**
 * Component By Techdeed
 * 
 * View
 * 
 * Default View of the School Management Component
 * 
 * @category   View
 * @package    Com_School
 * @subpackage Default
 * @author     Gnanakeethan Balasubramaniam <gnanakeethan@gmail.com>
 * @copyright  2012 Gnanakeethan.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @version    GIT:<git_id>
 * @link       Techdeed.x10.mx
 * @since      0.0.1.0.a.0.0.1.a *   
 */

// No direct access
defined('_JEXEC') or die;

/**
 * Title
 * 
 * Adds title to the School Management Component
 * 
 * @category   Decorators
 * @package    Com_School
 * @subpackage Default
 * @author     Gnanakeethan Balasubramaniam <gnanakeethan@gmail.com>
 * @copyright  2012 Gnanakeethan.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       Techdeed.x10.mx
 */
class SchoolDecoratorTitle extends SchoolCore 
{
    public function addTitle($title,$format)
    {
        
    }
}