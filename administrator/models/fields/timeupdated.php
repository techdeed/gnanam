<?php
/**
 * Component By Techdeed
 * 
 * PHP version 5
 * 
 * @category   Models
 * @package    Com_School
 * @subpackage Configuration
 * @author     Gnanakeethan Balasubramaniam <gnanakeethan@gmail.com>
 * @copyright  2012 Gnanakeethan.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @version    GIT:<git_id>
 * @link       Techdeed.x10.mx
 * @since      0.0.1.0.a.0.0.1.a    
 */

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');

/**
 * Model
 * 
 * Model of Configuration Page
 * 
 * @category   Models
 * @package    Com_School
 * @subpackage Configuration-Field-TimeUpdated
 * @author     Gnanakeethan Balasubramaniam <gnanakeethan@gmail.com>
 * @copyright  2012 Gnanakeethan.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       Techdeed.x10.mx
 */
class JFormFieldTimeupdated extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'timeupdated';

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since	1.6
	 */
	protected function getInput()
	{
		// Initialize variables.
		$html = array();
        
        
		$old_time_updated = $this->value;
        if (!$old_time_updated) {
            $html[] = '-';
        } else {
            $jdate = new JDate($old_time_updated);
            $pretty_date = $jdate->format(JText::_('DATE_FORMAT_LC2'));
            $html[] = "<div>".$pretty_date."</div>";
        }
        $time_updated = date("Y-m-d H:i:s");
        $html[] = '<input type="hidden" name="'.$this->name.'" value="'.$time_updated.'" />';
        
		return implode($html);
	}
}