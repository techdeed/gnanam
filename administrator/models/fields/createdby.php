<?php
/**
 * Component By Techdeed
 * 
 * PHP version 5
 * 
 * @category   Models
 * @package    Com_School
 * @subpackage Configuration
 * @author     Gnanakeethan Balasubramaniam <gnanakeethan@gmail.com>
 * @copyright  2012 Gnanakeethan.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @version    GIT:<git_id>
 * @link       Techdeed.x10.mx
 * @since      0.0.1.0.a.0.0.1.a    
 */

defined('JPATH_BASE') or die;

/**
 * Model
 * 
 * Model of Configuration Page
 * 
 * @category   Models
 * @package    Com_School
 * @subpackage Configuration-Field-CreatedBy
 * @author     Gnanakeethan Balasubramaniam <gnanakeethan@gmail.com>
 * @copyright  2012 Gnanakeethan.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       Techdeed.x10.mx
 */
class JFormFieldCreatedby extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'createdby';

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since	1.6
	 */
	protected function getInput()
	{
		// Initialize variables.
		$html = array();
        
        
		//Load user
		$user_id = $this->value;
		if ($user_id) {
			$user = JFactory::getUser($user_id);
		} else {
			$user = JFactory::getUser();
			$html[] = '<input type="hidden" name="'.$this->name.'" value="'.$user->id.'" />';
		}
		$html[] = "<div>".$user->name." (".$user->username.")</div>";
        
		return implode($html);
	}
}