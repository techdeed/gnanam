<?php
/**
 * Component By Techdeed
 * 
 * PHP version 5
 * 
 * @category  Library 
 * @package   Com_School
 * @author    Gnanakeethan Balasubramaniam <gnanakeethan@gmail.com>
 * @copyright 2012 Gnanakeethan.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 * @version   GIT:<git_id>
 * @link      Techdeed.x10.mx
 * @since     0.0.1.0.a.0.0.1.a *   
 */

// No direct access
defined('_JEXEC') || die;

/**
 * View
 * 
 * Default View of the School Management Component
 * 
 * @category   View
 * @package    Com_School
 * @subpackage Default
 * @author     Gnanakeethan Balasubramaniam <gnanakeethan@gmail.com>
 * @copyright  2012 Gnanakeethan.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       Techdeed.x10.mx
 */

class SchoolViewSchool extends JView
{
	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
        $module= JModuleHelper::getModule('mod_title');
            $module->position='hidden';
        $module= JModuleHelper::getModule('mod_toolbar');
            $module->position='hidden';    
        $module= JModuleHelper::getModule('mod_menu');
            $module->position='hidden';
		$module= JModuleHelper::getModule('mod_submenu');
			$module->position='hidden';
		$module= JModuleHelper::getModule('mod_status');
			$module->position='hidden';
		$module= JModuleHelper::getModule('mod_version');
			$module->position='hidden';    
        JToolBarHelper::title(JText::_('COM_SCHOOL'),'com_school_title');
		$document=&JFactory::getDocument();
        
        
        
        //JRequest::setVar('hidemainmenu', true);

        
        parent::display($tpl);
    }
}
    