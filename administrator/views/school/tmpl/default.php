<?php
/**
 * Component By Techdeed
 * 
 * View
 * 
 * Default View of the School Management Component
 * 
 * @category   View
 * @package    Com_School
 * @subpackage Default
 * @author     Gnanakeethan Balasubramaniam <gnanakeethan@gmail.com>
 * @copyright  2012 Gnanakeethan.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @version    GIT:<git_id>
 * @link       Techdeed.x10.mx
 * @since      0.0.1.0.a.0.0.1.a   
 */

// No direct access
defined('_JEXEC') ||  die;

/**
 * View
 * 
 * Default View of the School Management Component
 * 
 * @category   View
 * @package    Com_School
 * @subpackage Default
 * @author     Gnanakeethan Balasubramaniam <gnanakeethan@gmail.com>
 * @copyright  2012 Gnanakeethan.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       Techdeed.x10.mx
 */
$document=& JFactory::getDocument();
$document->addStyleSheet('components/com_school/assets/css/school/school.css');
?> 
<div id="mainblock" class="mainblock">
   <div id="header" class="header">        
    </div>    
    <div id="sidebar" class="sidebar">    
    </div>
    <div id="body" class="body">    
    </div>
    <div id="footer" class="footer">
    </div>
</div>