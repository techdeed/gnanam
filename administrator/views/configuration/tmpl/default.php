<?php
/**
 * Component By Techdeed
 * 
 * PHP version 5
 * 
 * @category   Views
 * @package    Com_School
 * @subpackage Configuration- Default Template
 * @author     Gnanakeethan Balasubramaniam <gnanakeethan@gmail.com>
 * @copyright  2012 Gnanakeethan.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @version    GIT:<git_id>
 * @link       Techdeed.x10.mx
 * @since      0.0.1.0.a.0.0.1.a    
 */

// no direct access
defined('_JEXEC') or die;

// Importing CSS.
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_school/assets/css/school.css');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'configuration.cancel' || document.formvalidator.isValid(document.id('configuration-form'))) {
			Joomla.submitform(task, document.getElementById('configuration-form'));
		}
		else {
			alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_school&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="configuration-form" class="form-validate">
	<div class="width-60 fltlft">
		<fieldset class="adminform">
			<legend><?php echo JText::_('COM_SCHOOL_LEGEND_CONFIGURATION'); ?></legend>
			<ul class="adminformlist">
                
				<li><?php echo $this->form->getLabel('id'); ?>
				<?php echo $this->form->getInput('id'); ?></li>
				<li><?php echo $this->form->getLabel('school_name'); ?>
				<?php echo $this->form->getInput('school_name'); ?></li>
				<li><?php echo $this->form->getLabel('school_address'); ?>
				<?php echo $this->form->getInput('school_address'); ?></li>


            </ul>
		</fieldset>
	</div>


	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
	<div class="clr"></div>

    
    <?php
    $c_style ='.adminformlist li {
            clear: both;
        }';
    JFactory::getDocument()->addStyleDeclaration($c_style, $type='text/css')
    ?>
</form>