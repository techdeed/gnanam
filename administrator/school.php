<?php
/**
 * Component By Techdeed
 * 
 * PHP version 5
 * 
 * @category  Admin
 * @package   Com_School
 * @author    Gnanakeethan Balasubramaniam <gnanakeethan@gmail.com>
 * @copyright 2012 Gnanakeethan.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 * @version   GIT:<git_id>
 * @link      Techdeed.x10.mx
 * @since     0.0.1.0.a.0.0.1.a    
 */

// No direct access.
defined('_JEXEC') || die;
$doc=&JFactory::getDocument();

//Adding Dojo ToolKit
$doc->addScript('components/com_school/assets/js/dojo/dojo-config.js','text/javascript');
$doc->addScript('components/com_school/assets/js/dojo/dojo/dojo.js','text/javascript');
$doc->addScript('components/com_school/assets/js/modernizr/modernizr.js','text/javascript');


JLoader::discover('J', JPATH_LIBRARIES.'/joomla', false, true);
JLoader::registerPrefix('School', $path=JPATH_COMPONENT_ADMINISTRATOR.'/library/School');

SchoolLessCompiler::autoCompileLess('school.less', 'school.css');
// Access check.
if (JFactory::getUser()->authorise('core.manage', 'com_school') === false) {
    throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

$controller	= JController::getInstance('School');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();

?>
<script src="components/com_school/assets/js/jquery/jquery-1.9.0.min.js" type="text/javascript" async></script>
<script src="components/com_school/assets/js/jquery-tools/jquery.tools.min.js" type="text/javascript"  async></script>
<script src="components/com_school/assets/js/jquery-ui/jquery-ui-1.10.0.min.js" type="text/javascript"  async></script>
